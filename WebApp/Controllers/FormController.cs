﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Controllers
{
    public class FormController : Controller
    {
        public ActionResult Form()
        {
            return View("FormPage", HttpContext.Application["forms"]);
        }

        [HttpPost]
        public ActionResult FormPage(string name, string genre,
            bool wantScifi, bool wantMobileVersion)
        {
            (HttpContext.Application["forms"] as List<(string, string, bool, bool)>)
                .Add((name, genre, wantScifi, wantMobileVersion));
            return RedirectToAction("FormPage");
        }

        [HttpGet]
        public ActionResult FormPage()
        {
            return View("FormPageResult", HttpContext.Application["forms"]);
        }


    }
}