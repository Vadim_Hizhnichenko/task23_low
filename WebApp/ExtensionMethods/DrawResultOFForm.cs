﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.ExtensionMethods
{
    public static class DrawResultsOFForm
    {
        public static MvcHtmlString DrawResults(this HtmlHelper html,
            List<(string name, string genre, bool wantScifi,
                bool wantMobileVersion)> data)
        {
            var numPeopeCertainGenre = new TagBuilder("ul");
            numPeopeCertainGenre.SetInnerText("Number of people, which prefer a certain genre:");

            var fantasticCount = data
                .Where(x => x.genre.Equals("Fantastic"))
                .Count();
            var fantasticText = new TagBuilder("li");
            fantasticText.SetInnerText($"Fantastic : {fantasticCount}");
            numPeopeCertainGenre.InnerHtml += fantasticText.ToString();

            var detectivText = new TagBuilder("li");
            var detectivCount = data
                .Where(x => x.genre.Equals("Detectives"))
                .Count();
            detectivText.SetInnerText($"Detectives : {detectivCount}");
            numPeopeCertainGenre.InnerHtml += detectivText.ToString();

            var historyText = new TagBuilder("li");
            var historyCount = data
                .Where(x => x.genre.Equals("History"))
                .Count();
            historyText.SetInnerText($"History : {historyCount}");
            numPeopeCertainGenre.InnerHtml += historyText.ToString();

            var adventureText = new TagBuilder("li");
            var adventureCount = data
                .Where(x => x.genre.Equals("Adventure"))
                .Count();
            adventureText.SetInnerText($"Adventure: {adventureCount}");
            numPeopeCertainGenre.InnerHtml += adventureText.ToString();

            var whatPeopleWantText = new TagBuilder("ul");
            whatPeopleWantText.SetInnerText("What people want:");

            var wantSciFiCount = data.Where(x => x.wantScifi)
                .Count();

            var SciFiText = new TagBuilder("li");
            SciFiText.SetInnerText($"{wantSciFiCount} people want more science fiction books");
            whatPeopleWantText.InnerHtml += SciFiText.ToString();

            var wantMobileVersionCount = data.Where(x => x.wantMobileVersion)
                .Count();

            var mobileVersText = new TagBuilder("li");
            mobileVersText.SetInnerText($"{wantMobileVersionCount} people want a mobile version of this site");
            whatPeopleWantText.InnerHtml += mobileVersText.ToString();

            var output = numPeopeCertainGenre.ToString() + whatPeopleWantText.ToString();

            return new MvcHtmlString(output);
        }
    }

}